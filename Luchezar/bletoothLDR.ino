#define TX 2
#define RX 3
#define LDR A6
#include<SoftwareSerial.h>
SoftwareSerial bluetooth(RX, TX);


void setup() {
  Serial.begin(9600);
  bluetooth.begin(9600);
  pinMode(A6, OUTPUT);
}

void loop() {
  int answer = 0;
  for(int conter=0; conter<9; conter++)
  {
    answer += analogRead(A6);
  }
  
  bluetooth.println(answer / 10);
  delay(100); 
}
