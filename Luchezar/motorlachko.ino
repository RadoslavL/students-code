#define BT_RX 3
#define BT_TX 2
#define LED_PIN 13

#define M1A 11
#define M1B 5       //дефинираме пиновете на моторите
#define M2A 9       //дефинираме пиновете на моторите
#define M2B 10

#include <SoftwareSerial.h>
SoftwareSerial bluetooth(BT_RX, BT_TX);

#include "VirtuinoBluetooth.h"
VirtuinoBluetooth virtuino(bluetooth);

void forward() {
  digitalWrite(M1A, HIGH);
  digitalWrite(M1B, LOW);
  digitalWrite(M2A, HIGH);
  digitalWrite(M2B, LOW);
}


void backward() {
  digitalWrite(M1A, LOW);
  digitalWrite(M1B, HIGH);
  digitalWrite(M2A, LOW);
  digitalWrite(M2B, HIGH);
}
void left() {
  digitalWrite(M1A, HIGH);
  digitalWrite(M1B, LOW);
  digitalWrite(M2A, LOW);
  digitalWrite(M2B, HIGH);
}
void right() {
  digitalWrite(M1A, LOW);
  digitalWrite(M1B, HIGH);
  digitalWrite(M2A, HIGH);
  digitalWrite(M2B, LOW);
}
void stopm() {
  digitalWrite(M1A, LOW);
  digitalWrite(M1B, LOW);
  digitalWrite(M2A, LOW);
  digitalWrite(M2B, LOW);
}

void setup() {
  bluetooth.begin(9600);
  Serial.begin(9600);
  pinMode(LED_PIN, OUTPUT);

}

void loop() {
  if (bluetooth.available()) {
    Serial.write(bluetooth.read());
  }
  if (Serial.available()) {
    bluetooth.write(Serial.read());
  }
  virtuino.run();
  if (virtuino.vMemoryRead(1) == 1) {
    
  }
  if (virtuino.vMemoryRead(2) == 1) {
    
  }
}