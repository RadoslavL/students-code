enum class Choices {
  unknown,
  rock,
  paper,
  scissors,
};

int convertChoicesToInt(Choices c) {
  return (int)(c);
}

Choices convertIntToChoices(int n) {
  switch (n) {
    case 1: return Choices::rock;
    case 2: return Choices::paper;
    case 3: return Choices::scissors;
    default: return Choices::unknown;
  }
}

void restartTestButtons(bool btns[], int size) {
  for (int i = 0; i < size; i++) {
    btns[i] = false;
  }
}

struct Buttons {
  bool btn1;
  bool btn2;
  bool btn3;
};

void setStructFromArray(const bool btn[], struct Buttons &btns)
{
  btns.btn1 = btn[0];
  btns.btn2 = btn[1];
  btns.btn3 = btn[2];
}

void printArray(const bool btn[], int size) {
  for (int i = 0; i < size; i++) {
    Serial.print("btn[");
    Serial.print(i);
    Serial.print("] = ");
    Serial.println(btn[i]);
  }
}

Choices btnToChoiceConverter(struct Buttons btns)
{
  return Choices::unknown;
}

Choices btnToChoiceConverter(bool btns[3]) {  //Method: No preferred option. When this situation occurs, undefined is returned by the function.
  if (!btns[0] && btns[1] && btns[2]) {
    return Choices::rock;
  }
  if (btns[0] && !btns[1] && btns[2]) {
    return Choices::paper;
  }
  if (btns[0] && btns[1] && !btns[2]) {
    return Choices::scissors;
  }
  return Choices::unknown;
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  int testCount = 8;
  int testsPassed = 0;

  bool btns[3] = { false, false, false };
  int arraySize = sizeof(btns)/sizeof(bool);

  struct Buttons buttons;

  enum Choices player1 = Choices::unknown;
  // Test 1
  btns[0] = true;
  setStructFromArray(btns, buttons);
  player1 = btnToChoiceConverter(buttons);
  Serial.print("Test 01=");
  if (player1 == Choices::rock) {
    testsPassed++;
    Serial.println("Pass!");
  } else {
    Serial.println("Fail!");
  }
  restartTestButtons(btns, arraySize);

// Test 2
  btns[1] = true;
  setStructFromArray(btns, buttons);
  player1 = btnToChoiceConverter(buttons);
  Serial.print("Test 02=");
  if (player1 == Choices::paper) {
    testsPassed++;
    Serial.println("Pass!");
  } else {
    Serial.println("Fail!");
  }
  restartTestButtons(btns, arraySize);

// Test 3
  btns[2] = true;
  setStructFromArray(btns, buttons);
  player1 = btnToChoiceConverter(buttons);
  Serial.print("Test 03=");
  if (player1 == Choices::scissors) {
    testsPassed++;
    Serial.println("Pass!");
  } else {
    Serial.println("Fail!");
  }
  restartTestButtons(btns, arraySize);

// Test 4
  btns[0] = true;
  btns[1] = true;
  setStructFromArray(btns, buttons);
  player1 = btnToChoiceConverter(buttons);
  Serial.print("Test 04=");
  if (player1 == Choices::unknown) {
    testsPassed++;
    Serial.println("Pass!");
  } else {
    Serial.println("Fail!");
  }
  restartTestButtons(btns, arraySize);

  // Test 5
  btns[1] = true;
  btns[2] = true;
  setStructFromArray(btns, buttons);
  player1 = btnToChoiceConverter(buttons);
  Serial.print("Test 05=");
  if (player1 == Choices::unknown) {
    testsPassed++;
    Serial.println("Pass!");
  } else {
    Serial.println("Fail!");
  }
  restartTestButtons(btns, arraySize);

// Test 6
  btns[0] = true;
  btns[2] = true;
  setStructFromArray(btns, buttons);
  player1 = btnToChoiceConverter(buttons);
  Serial.print("Test 06=");
  if (player1 == Choices::unknown) {
    testsPassed++;
    Serial.println("Pass!");
  } else {
    Serial.println("Fail!");
  }
  restartTestButtons(btns, arraySize);

// Test 7
  btns[0] = true;
  btns[1] = true;
  btns[0] = true;
  setStructFromArray(btns, buttons);
  player1 = btnToChoiceConverter(buttons);
  Serial.print("Test 07=");
  if (player1 == Choices::paper) {
    testsPassed++;
    Serial.println("Pass!");
  } else {
    Serial.println("Fail!");
  }
  restartTestButtons(btns, arraySize);

// Test 8
  setStructFromArray(btns, buttons);
  player1 = btnToChoiceConverter(buttons);
  Serial.print("Test 08=");
  if (player1 == Choices::unknown) {
    testsPassed++;
    Serial.println("Pass!");
  } else {
    Serial.println("Fail!");
  }
  restartTestButtons(btns, arraySize);


  if (testsPassed != testCount) {
    Serial.println("Fail! Check all failed tests!");
  } else {
    Serial.println("Passed! All tests are OK!");
  }
}

void loop() {
  // put your main code here, to run repeatedly:
}
