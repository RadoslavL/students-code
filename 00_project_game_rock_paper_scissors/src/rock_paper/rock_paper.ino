
class myClass
{

}; 

struct buttons {
  bool btn1;
  bool btn2;
  bool btn3;
} myStruct;

enum class Choices {
  unknown,
  rock,
  paper,
  scissors,
};

bool btns_player1[3];

// Choices btnToChoiceConverter(bool btn1, bool btn2, bool btn3)

// Choices btnToChoiceConverter(struct buttons btns)

// Choices btnToChoiceConverter(bool btns[])
Choices btnToChoiceConverter(bool btns[3])
{ //Method: No preferred option. When this situation occurs, undefined is returned by the function.
  if(!btns[0] && btns[1] && btns[2]){
    return Choices::rock;
  }
  if(btns[0] && !btns[1] && btns[2]){
    return Choices::paper;
  }
  if(btns[0] && btns[1] && !btns[2]){
    return Choices::scissors;
  }
  return Choices::unknown;
}


int convertChoicesToInt(Choices c)
{

    return (int)(c);
}

Choices convertIntToChoices(int n)
{
  switch (n)
  {
    case 1 : return Choices::rock;
    case 2 : return Choices::paper;
    case 3 : return Choices::scissors;
    default: return Choices::unknown;
  }

  if (n == 1) return Choices::paper;
  else if (n == 2) return Choices::rock;
}
void setup() {
  // put your setup code here, to run once:
 enum Choices player1 = Choices::scissors;
Serial.begin(9600);

bool btns[3] = {false, false, false};
btns[0] = true;

player1 = btnToChoiceConverter(btns);

Serial.print("My choice=");
Serial.println(convertChoicesToInt(player1));

}

void loop() {
  // put your main code here, to run repeatedly:

}
