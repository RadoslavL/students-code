# План за работа 

## 01 занятие 2024.10.05

1. Запознаване на групата с проекта

## 02 занятие 2024.10.12

1. Проверка на 3D дизайн
    - напасване на отвор за усилване на говорител
    - проверка и корекция на основа за електрониката

## 03 занятие 2024.10.19

1. Електроника
    - да се напаснат държачи за светодиод 
    - да се запоят резистори със светодиодите
    - да използваме ли резистори за бутоните
1. 3D дизайн
    - проектиране на капачка за бутоните, принтиране на прототип за тест


Необходими материали:
- прототипна платка(еднослойна), от която да се изрязват парчета
- режещи инструменти
    - резци малки и големи
    - трион
- шкурка, пила за изглаждане на ръбове 
- поялник
- тинол
- жици
- светодиоди
- резистори да се избере правилна стойност
- 3D принтер
- филамент 
- силиконов пистолет и силикон
- моментно лепило

Резултат:
- преговор на свързване на светодиоди и смятане на стойност на съпротивлението на резистора -  R = 820 Ohm
- обсъждане на схема за свързване на бутон

## 04 занятие 2024.10.26

1. Електроника
    - да се напаснат държачи за светодиод
    - да се запоят резистори със светодиодите
    - да използваме ли резистори за бутоните, да се измери
1. 3D дизайн
    - проектиране на капачка за бутоните, принтиране на прототип за тест


Необходими материали:
- прототипна платка(еднослойна), от която да се изрязват парчета
- режещи инструменти
    - резци малки и големи
    - трион
- шкурка, пила за изглаждане на ръбове
- поялник
- тинол
- жици
- светодиоди
- резистори да се избере правилна стойност
- 3D принтер
- филамент
- силиконов пистолет и силикон
- моментно лепило

Резултат:
- схема за свързване на светодиодите и бутоните

## 05 занятие 09.11.2024
1. Електроника
    - да се изработи платката за диоди и бутони
1. 3D дизайн
    - проектиране на капачка за бутоните, принтиране на прототип за тест
    - да се направят държачи за платката за бутони и светодиоди в 02_cap_box


Необходими материали:
- прототипна платка(еднослойна), от която да се изрязват парчета
- режещи инструменти
    - резци малки и големи
    - трион
- шкурка, пила за изглаждане на ръбове
- поялник
- тинол
- жици
- светодиоди
- резистори да се избере правилна стойност
- 3D принтер
- филамент
- силиконов пистолет и силикон
- моментно лепило

## 06 занятие

## 07 занятие

## 08 занятие

## 09 занятие

## 10 занятие

## 11 занятие

## 12 занятие