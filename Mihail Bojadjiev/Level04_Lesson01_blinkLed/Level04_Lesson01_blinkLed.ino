#define LED_YELLOW    (13)
void setup() {
  pinMode(LED_YELLOW, OUTPUT);

}

void loop() {
  digitalWrite(LED_YELLOW, HIGH);
  delay(500);
  digitalWrite(LED_YELLOW, LOW);
  delay(500);
}
