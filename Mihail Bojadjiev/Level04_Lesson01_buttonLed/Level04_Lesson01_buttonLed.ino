#define LED_YELLOW    (13)
#define BUTTON_PIN   (12)

void setup() {
  pinMode(LED_YELLOW, OUTPUT);
  pinMode(BUTTON_PIN, INPUT);

}

void loop() {
  bool  isBtnPressd;
  isBtnPressd = digitalRead (BUTTON_PIN);
  if (isBtnPressd) {
    digitalWrite(LED_YELLOW, HIGH);
  }

  else {
    digitalWrite(LED_YELLOW, LOW);
  }
}
