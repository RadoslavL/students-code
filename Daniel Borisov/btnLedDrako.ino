#define BUTTON 12
#define LED 13
bool button = 0;

void setup() {
  pinMode(BUTTON, INPUT);
  pinMode(LED, OUTPUT);
}

void loop() {
  bool ButtonPress = digitalRead(BUTTON);
  
  if (button == 0 && ButtonPress == 1){
    digitalWrite(LED, HIGH);
    button = 1;
  }
  
  if (button == 1 && ButtonPress == 1){
    digitalWrite(LED, LOW);
    button = 0;
  }
}
