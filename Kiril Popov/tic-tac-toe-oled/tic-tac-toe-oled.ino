#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SH110X.h>
#include <Adafruit_NeoPixel.h>
#include "mega2560_pinout.h"

#define TCAADDR 0x70
#define OLED_Address 0x3C  //initialize with the I2C addr 0x3C
#define SCREEN_WIDTH 128   // OLED display width, in pixels
#define SCREEN_HEIGHT 64   // OLED display height, in pixels
#define OLED_RESET -1      //   QT-PY / XIAO

#define num 8
#define mtrxPin DP02
#define MIN_RED_ANALOG_VALUE 900
#define MAX_RED_ANALOG_VALUE 1023
#define MIN_GREEN_ANALOG_VALUE 700
#define MAX_GREEN_ANALOG_VALUE 800

Adafruit_SH1106G display = Adafruit_SH1106G(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);
Adafruit_NeoPixel matrix(64, mtrxPin, NEO_GRB + NEO_KHZ800);

const int BOARD_SIZE = 3;

int pinout[3][3] = {
  { AP00, AP02, AP04 },
  { AP06, AP08, AP10 },
  { AP12, AP14, AP09 }
};

const int numberOfRows = 3;
const int numberOfCols = 3;

char boardState[numberOfRows][numberOfCols] = { { '2', '1', '2' },
                                                { '2', '1', '1' },
                                                { '1', '1', '2' } };

uint32_t red = matrix.Color(80, 0, 0);
uint32_t green = matrix.Color(0, 80, 0);
uint32_t blue = matrix.Color(0, 0, 80);
uint32_t white = matrix.Color(80, 80, 80);

const char pawn_red_state = '1';
const char pawn_green_state = '2';
const char pawn_off_state = '-';

void TheMatrix(char boardState[3][3]);

void readPins(int pinout[numberOfRows][numberOfCols], char boardState[numberOfRows][numberOfCols]) {
  int currentread;
  for (int row = 0; row < numberOfRows; row++) {
    for (int col = 0; col < numberOfCols; col++) {
      currentread = analogRead(pinout[row][col]);
      if (currentread < MIN_GREEN_ANALOG_VALUE) {
        boardState[row][col] = pawn_off_state;
      }
      if (currentread > MIN_RED_ANALOG_VALUE && currentread < MAX_RED_ANALOG_VALUE) {
        boardState[row][col] = pawn_red_state;
      }
      if (currentread > MIN_GREEN_ANALOG_VALUE && currentread < MAX_GREEN_ANALOG_VALUE) {
        boardState[row][col] = pawn_green_state;
      }
    }
  }
}

void I2C_MUX_Channel_Select(uint8_t i) {
  if (i > 7) return;

  Wire.beginTransmission(TCAADDR);
  Wire.write(1 << i);
  Wire.endTransmission();
}


void setup() {
  pinMode(AP00, INPUT);
  pinMode(AP02, INPUT);
  pinMode(AP04, INPUT);
  pinMode(AP06, INPUT);
  pinMode(AP08, INPUT);
  pinMode(AP10, INPUT);
  pinMode(AP12, INPUT);
  pinMode(AP14, INPUT);
  pinMode(AP09, INPUT);
  pinMode(mtrxPin, OUTPUT);

  Serial.begin(9600);
  matrix.begin();
  Wire.begin();

  matrix.setBrightness(25);
  I2C_MUX_Channel_Select(1);
  delay(250);                                // wait for the OLED to power up
  if (!display.begin(OLED_Address, true)) {  // Address 0x3C default
    return;
  }
  display.display();
  delay(1);
  display.clearDisplay();

  display.setTextSize(1);
  display.setTextColor(SH110X_WHITE);
  display.setCursor(20, 10);
}

bool active_game(char State[3][3]) {
  bool no_more_turns = 0;
  for (int i = 0; i < BOARD_SIZE; i++) {
    for (int j = 0; j < BOARD_SIZE; j++) {
      if (boardState[i][j] == pawn_off_state) {
        no_more_turns = 1;
        return true;
      }
    }
    if (no_more_turns == 0) {
      return false;
    }
  }
}
void testdrawrect() {
  //  for (int i = 0; i < 2; i++ ) {
  display.clearDisplay();
  display.drawRect(10, 10, 30, 50, SH110X_WHITE);
  display.display();
  delay(1000);
  display.clearDisplay();
  display.drawRect(20, 20, 20, 40, SH110X_WHITE);
  display.display();
  delay(1000);
}

void loop() {
  unsigned long startTime = millis();
  bool isChanged = false;
  display.setCursor(20, 10);
  display.clearDisplay();
  readPins(pinout, boardState);
  if (active_game(boardState)) {
    if (isBoardValid(boardState)) {
      updateMatrix(boardState);
      // Determine the winner
      char winner = determineWinner(boardState);
      if (winner == pawn_red_state) {
        Serial.println("X wins!");
        display.println("X wins!");

      } else if (winner == pawn_green_state) {
        Serial.println("O wins!");
        display.println("O wins!");

      } else {
        Serial.println("Game continues");
        display.setCursor(20, 10);
        display.println("Game continues");
      }
    }
  } else {
    char winner = determineWinner(boardState);
    if (winner == pawn_red_state) {
      Serial.println("X wins!");
      display.println("X wins!");

    } else if (winner == pawn_green_state) {
      Serial.println("O wins!");
      display.println("O wins!");

    } else {
      Serial.println("It's a draw!");
      display.println("It's a draw!");
    }
  }
  //testdrawrect();
  if (isChanged)
    display.display();

  unsigned long delta = millis() - startTime;
  Serial.println(delta);
  delay(1000);

}
// Serial.println(analogRead(AP00));


void setSquareColor(int row, int col, char player) {
  int mtrxRow = row * 3;
  int mtrxCol = col * 3;

  int pixel1 = num * mtrxRow + mtrxCol;
  int pixel2 = num * mtrxRow + mtrxCol + 1;
  int pixel3 = num * (mtrxRow + 1) + mtrxCol;
  int pixel4 = num * (mtrxRow + 1) + mtrxCol + 1;


  if (player == pawn_red_state) {
    matrix.setPixelColor(pixel1, red);
    matrix.setPixelColor(pixel2, red);
    matrix.setPixelColor(pixel3, red);
    matrix.setPixelColor(pixel4, red);
  } else if (player == pawn_green_state) {
    matrix.setPixelColor(pixel1, green);
    matrix.setPixelColor(pixel2, green);
    matrix.setPixelColor(pixel3, green);
    matrix.setPixelColor(pixel4, green);
  } else if (player == pawn_off_state) {
    matrix.setPixelColor(pixel1, white);
    matrix.setPixelColor(pixel2, white);
    matrix.setPixelColor(pixel3, white);
    matrix.setPixelColor(pixel4, white);
  }
  matrix.show();
}

void updateMatrix(char State[3][3]) {
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      setSquareColor(i, j, State[i][j]);
    }
  }
}

bool isBoardValid(char State[3][3]) {
  int red_squares_num = 0;
  int green_squares_num = 0;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      if (State[i][j] == pawn_green_state) {
        green_squares_num += 1;
      } else if (State[i][j] == pawn_red_state) {
        red_squares_num += 1;
      }
    }
    if (green_squares_num == red_squares_num) {
      return true;
    } else if (green_squares_num - red_squares_num == 1) {
      return true;
    } else {
      return false;
    }
  }
}
// Function to determine the winner
char determineWinner(char board[3][3]) {
  // Check rows
  for (int i = 0; i < BOARD_SIZE; i++) {
    if (board[i][0] != pawn_off_state && board[i][0] == board[i][1] && board[i][0] == board[i][2]) {
      return board[i][0];
    }
  }

  // Check columns
  for (int j = 0; j < BOARD_SIZE; j++) {
    if (board[0][j] != pawn_off_state && board[0][j] == board[1][j] && board[0][j] == board[2][j]) {
      return board[0][j];
    }
  }

  // Check diagonals
  if (board[0][0] != pawn_off_state && board[0][0] == board[1][1] && board[0][0] == board[2][2]) {
    return board[0][0];
  }

  if (board[0][2] != pawn_off_state && board[0][2] == board[1][1] && board[0][2] == board[2][0]) {
    return board[0][2];
  }

  // No winner
  return '0';
}