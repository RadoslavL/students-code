cmake_minimum_required(VERSION 3.10)

project(myProj 
        VERSION 1.0 
        LANGUAGES CXX)

# specify the C++ standard
set(CMAKE_CXX_STANDARD 11)
# set(CMAKE_CXX_STANDARD_REQUIRED True)

# add_subdirectory(common)

add_executable(${PROJECT_NAME})
target_sources(${PROJECT_NAME}
               PRIVATE
               task.cpp
)

