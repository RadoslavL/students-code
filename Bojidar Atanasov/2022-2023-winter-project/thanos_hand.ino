// #define LED_RED (13)
// #define button_PIN (12)
#include <Adafruit_NeoPixel.h>

#define WP24 (24) 
#define DP24 WP24
#define RGB_LED_MATRIX_PIN WP24


const int rgbMatrixPixelsCount = 8;
Adafruit_NeoPixel rgbMatrix = Adafruit_NeoPixel(rgbMatrixPixelsCount, RGB_LED_MATRIX_PIN, NEO_GRB + NEO_KHZ800);

#define PowerStone_pin (7)
#define SpaceStone_pin (2)
#define RealityStone_pin (3)
#define SoulStone_pin (4)
#define TimeStone_pin (5)
#define MindStone_pin (6)

uint32_t PowerStoneColor = rgbMatrix.Color(250, 0, 250);
uint32_t SpaceStoneColor = rgbMatrix.Color(0, 255, 0);
uint32_t RealityStoneColor = rgbMatrix.Color(255, 0, 0);
uint32_t SoulStoneColor = rgbMatrix.Color(255, 50, 0);
uint32_t TimeStoneColor = rgbMatrix.Color(255, 0, 0);
uint32_t MindStoneColor = rgbMatrix.Color(0, 255, 0);
uint32_t OffColor = rgbMatrix.Color(0, 0, 0);

void setup() {
  pinMode(PowerStone_pin, INPUT);
  pinMode(SpaceStone_pin, INPUT);
  pinMode(RealityStone_pin, INPUT);
  pinMode(SoulStone_pin, INPUT);
  pinMode(TimeStone_pin, INPUT);
  pinMode(MindStone_pin, INPUT);
  rgbMatrix.begin();
  rgbMatrix.setBrightness(50);
}

void loop() {
  bool isPowerStoneActive = digitalRead(PowerStone_pin);
  bool isSpaceStoneActive = digitalRead(SpaceStone_pin);
  bool isRealityStoneActive = digitalRead(RealityStone_pin);
  bool isSoulStoneActive = digitalRead(SoulStone_pin);
  bool isTimeStoneActive = digitalRead(TimeStone_pin);
  bool isMindStoneActive = digitalRead(MindStone_pin);

  if (isPowerStoneActive) {
    rgbMatrix.setPixelColor(0, PowerStoneColor);
  } else {
    rgbMatrix.setPixelColor(0, OffColor);
  }
  if (isSpaceStoneActive) {
    rgbMatrix.setPixelColor(1, SpaceStoneColor);
  } else {
    rgbMatrix.setPixelColor(1, OffColor);
  }
  if (isRealityStoneActive) {
    rgbMatrix.setPixelColor(2, RealityStoneColor);
  } else {
    rgbMatrix.setPixelColor(2, OffColor);
  }
  if (isSoulStoneActive) {
    rgbMatrix.setPixelColor(3, SoulStoneColor);
  } else {
    rgbMatrix.setPixelColor(3, OffColor);
  }
  if (isTimeStoneActive) {
    rgbMatrix.setPixelColor(4, TimeStoneColor);
  } else {
    rgbMatrix.setPixelColor(4, OffColor);
  }
  if (isMindStoneActive) {
    rgbMatrix.setPixelColor(5, MindStoneColor);
  } else {
    rgbMatrix.setPixelColor(5, OffColor);
  }
  rgbMatrix.show();
}
