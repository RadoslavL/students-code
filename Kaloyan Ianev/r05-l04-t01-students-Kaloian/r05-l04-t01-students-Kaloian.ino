#include "VirtuinoBluetooth.h"
#include "mega2560_pinout.h"
VirtuinoBluetooth virtuino(Serial1);
void setup() {
  Serial1.begin(9600);
  pinMode(RED_LED_PIN, OUTPUT);
  pinMode(GREEN_LED_PIN, OUTPUT);
  pinMode(BLUE_LED_PIN, OUTPUT);
}
void loop() {
  virtuino.run();
  if (virtuino.vMemoryRead(0) == 1) {
    digitalWrite(BLUE_LED_PIN, HIGH);
  }
  else {
    digitalWrite(BLUE_LED_PIN, LOW);
  }
if (virtuino.vMemoryRead(1) == 1) {
  digitalWrite(GREEN_LED_PIN, HIGH);
}
else {
  digitalWrite(GREEN_LED_PIN, LOW);
}

if (virtuino.vMemoryRead(3) == 1) {
  digitalWrite(RED_LED_PIN, HIGH);
}
else {
  digitalWrite(RED_LED_PIN, LOW);
}
}
