#define LED 13

void setup() {
  pinMode(13, OUTPUT);
}

void loop() {
  sos();
  delay(700);
  Boris();
  delay(700);
}

void sos() {
  s();
  delay(300);
  o();
  delay(300);
  s();

}
void Boris() {
  b();
  delay(100);
  o();
  delay(100);
  delay(100);
  r();
  delay(100);
  i();
  delay(100);
  s();

}
void dot() {
  digitalWrite(LED, HIGH);
  delay(100);
  digitalWrite(LED, LOW);
}

void dash() {
  digitalWrite(LED, HIGH);
  delay(300);
  digitalWrite(LED, LOW);
}

void s() {
  dot();
  delay(100);
  dot();
  delay(100);
  dot();
}

void o() {
  dash();
  delay(100);
  dash();
  delay(100);
  dash();
}

void b() {
  dash();
  delay(100);
  dot();
  delay(100);
  dot();
  delay(100);
  dot();
}
void r () {
  dot();
  delay(100);
  dash();
  delay(100);
  dot();
}
void i () {
  dot();
  delay(100);
  dot();
}
