#include <Adafruit_NeoPixel.h>

Adafruit_NeoPixel pixel (1, 16, NEO_GRB + NEO_KHZ800);

#define RGB_Matrix A0//SDA


#define Y 8
#define X 8


void setup() {
  // put your setup code here, to run once:
  pinMode(12, INPUT);
  pixel.begin();
  pixel.setBrightness(70);

  pixel.setPixelColor(0, 150, 140, 130);
  pixel.show();
}
int Button_State = 0;
int Pixel_Brightness = 0;

void loop() {
  pixel.setPixelColor(0, 150, 140, 130);
  pixel.show();

  if (digitalRead(12) == HIGH && Button_State == 0)
  {
    Button_State = 1;
    if (Pixel_Brightness >= 250)
    {
      Pixel_Brightness = 0;
    }
    else {
      Pixel_Brightness += 50;

    }
    pixel.setBrightness(Pixel_Brightness);

  }
  else if (digitalRead(12) == LOW)
  {
    Button_State = 0;
  }


}
