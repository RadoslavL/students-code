#include "mega2560_pinout.h"

// Initialize all needed pins and libraries
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial2.begin(9600);

  pinMode(SET_HC12_PIN, OUTPUT);
  pinMode(RED_LED_PIN, HIGH);
  pinMode(DIP1_PIN ,INPUT);
}

char commandStart = 'v';
char commandOn = '1';
char commandOff = '0';

char myChar = ' ';
bool print = 0;

// Write the code executed in a loop
void loop() {
  bool pin1 = digitalRead(DIP1_PIN);
  
  if (pin1 == HIGH) {
    digitalWrite(SET_HC12_PIN, HIGH);
    print = 1;
  } else {
    print = 0;
    digitalWrite(SET_HC12_PIN, LOW);
  }

  if (Serial.available()) {
    char read = Serial.read();
    Serial2.write(read);
    Serial.write(read);
  }

  if (Serial2.available()) {
    if (myChar == commandStart){
      myChar = Serial2.read();
      if (myChar == commandOn){
        digitalWrite(RED_LED_PIN, HIGH);       
      }
      else if (myChar == commandOff){
        digitalWrite(RED_LED_PIN, LOW);
      }
    }
    else{
      myChar = Serial2.read();
    }
    Serial.write(myChar);
    if (print){
      Serial2.write(myChar);      
    }
  }
}
