#define l_d_pin A1
#define g_d_pin A7
#define button A3

enum BallDir{
  UpLeft, UpRight, DownRight, DownLeft, Up, Down
};

unsigned long StartTime = 0;

#include <Adafruit_NeoPixel.h>
Adafruit_NeoPixel pixel(64, A0, NEO_GRB + NEO_KHZ800);
void setup() {
  pinMode(12, INPUT);
  pinMode(A0, OUTPUT);
  pixel.begin();
  pixel.setBrightness(15);

  pinMode(l_d_pin, INPUT);
  pinMode(g_d_pin, INPUT);
  pinMode(button, INPUT_PULLUP);

  Serial.begin(9600);

  StartTime = millis();
}

int pixelBrightness = 0;
const int red = 7;
int kolona = 2;
int red_b = 0;
int kolona_b = 0;

void upleft(){
  kolona_b--;
  red_b--;
}

void upright(){
  kolona_b++;
  red_b--;
}

void downright(){
  kolona_b++;
  red_b++;
}

void downleft(){
  kolona_b--;
  red_b++;
}

void up(){
  red_b--;
}

void down(){
  red_b++;
}

void loop() {
  unsigned long CurrentTime = millis();
  Serial.print("c - ");
  Serial.println(CurrentTime);
  unsigned long ElapsedTime = CurrentTime - StartTime;
  Serial.print("e - ");
  Serial.println(ElapsedTime);
  
  int l_d = analogRead(l_d_pin);
  int g_d = analogRead(g_d_pin);
  Serial.print("l_d - ");
  Serial.println(l_d);
  Serial.print("g_d - ");
  Serial.println(g_d);
  Serial.print("button - ");
  Serial.println(digitalRead(button));

  pixel.setPixelColor(8 * red + kolona, 0, 0, 0);
  pixel.setPixelColor(8 * red + (kolona+1), 0, 0, 0);
  pixel.setPixelColor(8 * red + (kolona+2), 0, 0, 0);
  pixel.setPixelColor(8 * (red_b-1) + kolona_b, 0, 0, 0);

  if ((kolona != 5) && (l_d >= 750)){
    kolona++;
    Serial.println("kolona++");
  }

  if ((kolona != 0) && (l_d <= 100)){
    kolona--;
    Serial.println("kolona--");
  }

  Serial.print(red);
  Serial.print(",");
  Serial.println(kolona);
  
  pixel.setPixelColor(8 * red + kolona, 255, 255, 255);
  pixel.setPixelColor(8 * red + (kolona+1), 255, 255, 255);
  pixel.setPixelColor(8 * red + (kolona+2), 255, 255, 255);
  pixel.setPixelColor(8 * red_b + kolona_b, 255, 0, 0);
  
  pixel.show();
  /*if (red_b == red - 1 && kolona_b == kolona && kolona_b != 0){
    upleft();
  }
  else if (red_b == red - 1 && kolona_b == kolona && kolona_b == 0){
    upright();
  }
  else if*/ 
  if (ElapsedTime >= 500 && (red_b != red-1 || (red_b == red - 1 && kolona_b != kolona))){
    red_b++;
    StartTime = CurrentTime;
  }
  Serial.print("s - ");
  Serial.print(StartTime);
  delay(130);
}
