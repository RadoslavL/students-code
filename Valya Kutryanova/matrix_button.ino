#include <Adafruit_NeoPixel.h>

Adafruit_NeoPixel matrix(64, A0, NEO_GBR + NEO_KHZ800);

void setup() {
  pinMode(12, INPUT);
  pinMode(A0, OUTPUT);
  matrix.begin();
  matrix.setBrightness(70);

}

int matrix_brightness = 0;
int button_state = 0;
int index = 0;

void loop() {
  if (index >= 64){
    matrix.clear();
    index =0;
  }
  
  if (digitalRead(12) == HIGH && button_state == 0) {
    button_state = 1;
    matrix.setPixelColor(index - 1, 0, 0, 0);
    matrix.setPixelColor(index, 255, 0, 0);
    matrix.show();
    index++;
  }
  else if (digitalRead(12) == LOW){
    button_state = 0;
  }
}
