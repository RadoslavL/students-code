#include <Adafruit_NeoPixel.h>
#include "mega2560_pinout.h"

#define num 8
#define mtrxPin AP14

Adafruit_NeoPixel matrix(64, mtrxPin, NEO_GRB + NEO_KHZ800);

int pinout[3][3] = {
  { AP00, AP02, AP04 },
  { AP06, AP08, AP10 },
  { AP12, SERVO2_PIN, SERVO1_PIN }
};

const int numberOfRows = 3;
const int numberOfCols = 3;
char boardState[numberOfRows][numberOfCols] = {{'2', '1', '2'}, 
                                               {'2', '1', '1'}, 
                                               {'1', '1', '2'}};

uint32_t red = matrix.Color(80, 0, 0);
uint32_t green = matrix.Color(0, 80, 0);
uint32_t blue = matrix.Color(0, 0, 80);
uint32_t white = matrix.Color(80, 80, 80);

const char p1 = '1';
const char p2 = '2';
const char off = '-';

void TheMatrix(char boardState[3][3]);

int currentread;

void readPins(int pinout[3][3], char boardState[3][3]) {
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      currentread = analogRead(pinout[i][j]);
      if (currentread > 1000) {
        boardState[i][j] = '-';
      }
      if (currentread > 600 && currentread < 1000) {
        boardState[i][j] = '1';
      }
      if (currentread < 600) {
        boardState[i][j] = '2';
      }
    }
  }
}

void setup() {
  pinMode(AP00, INPUT_PULLUP);
  pinMode(AP02, INPUT_PULLUP);
  pinMode(AP04, INPUT_PULLUP);
  pinMode(AP06, INPUT_PULLUP);
  pinMode(AP08, INPUT_PULLUP);
  pinMode(AP10, INPUT_PULLUP);
  pinMode(AP12, INPUT_PULLUP);
  pinMode(SERVO2_PIN, INPUT_PULLUP);
  pinMode(SERVO1_PIN, INPUT_PULLUP);
  pinMode(mtrxPin, OUTPUT);
  Serial.begin(9600);
  matrix.begin();
  matrix.setBrightness(25);
}

void loop() {
  readPins(pinout, boardState);
  TheMatrix(boardState);

}

void drawMatrix(int row, int col, char player){
  int mtrxRow = row * 3;
  int mtrxCol = col * 3;

  int pixel1 = num * mtrxRow + mtrxCol;
  int pixel2 = num * mtrxRow + mtrxCol + 1;
  int pixel3 = num * (mtrxRow + 1) + mtrxCol;
  int pixel4 = num * (mtrxRow + 1) + mtrxCol + 1;


  if (player == p1){
    matrix.setPixelColor(pixel1, green);
    matrix.setPixelColor(pixel2, green);
    matrix.setPixelColor(pixel3, green);
    matrix.setPixelColor(pixel4, green);   
  }
  else if (player == p2){
    matrix.setPixelColor(pixel1, red);
    matrix.setPixelColor(pixel2, red);
    matrix.setPixelColor(pixel3, red);
    matrix.setPixelColor(pixel4, red); 
  }
  else if (player == off){
    matrix.setPixelColor(pixel1, white);
    matrix.setPixelColor(pixel2, white);
    matrix.setPixelColor(pixel3, white);
    matrix.setPixelColor(pixel4, white); 
  }
  matrix.show();
}

void TheMatrix(char State[3][3]){
  for (int i = 0; i < 3; i++){
    for (int j = 0; j < 3; j++){
      drawMatrix(i, j, State[i][j]);
    }
  }
}