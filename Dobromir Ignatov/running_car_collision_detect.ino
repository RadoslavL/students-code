#define Echo 8
#define Trig 7
#define left1 11
#define left2 5
#define right1 9
#define right2 10
#define LED 13
#define Buzzer 6
#define DistToColision 15
#define LowSpeed 0
#define HighSpeed 140

void setup() {
  pinMode(Echo, INPUT);
  pinMode(Trig, OUTPUT);
  pinMode(left1, OUTPUT);
  pinMode(left2, OUTPUT);
  pinMode(right1, OUTPUT);
  pinMode(right2, OUTPUT);
  Serial.begin(9600);

}
void stop() {
  digitalWrite(left1, LOW);
  digitalWrite(left2, LOW);
  digitalWrite(right1, LOW);
  digitalWrite(right2, LOW);
}
void forward()
{
  analogWrite(left1, HighSpeed);
  digitalWrite(left2, LOW);
  analogWrite(right1, HighSpeed);
  digitalWrite(right2, LOW);
}

void right()
{
  analogWrite(left1, HighSpeed);
  digitalWrite(left2, LOW);
  digitalWrite(right1, LOW);
  analogWrite(right2, HighSpeed);
}

void left()
{
  digitalWrite(left1, LOW);
  analogWrite(left2, HighSpeed);
  analogWrite(right1, HighSpeed);
  digitalWrite(right2, LOW);
}
void backward()
{
  digitalWrite(left1, LOW);
  analogWrite(left2, HighSpeed);
  digitalWrite(right1, LOW);
  analogWrite(right2, HighSpeed);
}

int counter = 0;
int MeasureCounter = 0;

float calculate_distance() {
  digitalWrite(Trig, LOW);
  delayMicroseconds(10);
  digitalWrite(Trig, HIGH);
  delayMicroseconds(10);
  digitalWrite(Trig, LOW);

  float distance = pulseIn(Echo, HIGH);
  distance = distance * 0.034 / 2;
  return distance;
}

float distance = calculate_distance();
void loop() {
 MeasureCounter++;
 if (MeasureCounter = 10000){
  distance = calculate_distance();
  MeasureCounter = 0;
 }
 
 
  
  counter++;
  
  if (counter = 20000)
  {
    Serial.println(distance);
    counter = 0;
  }


  if (distance < DistToColision)
  {
    stop();
    right();
  }
  else
    forward();



  if (distance < DistToColision) {
    digitalWrite(LED, HIGH);
    analogWrite(Buzzer, 50);
  }
  else
  { digitalWrite(LED, LOW);
    analogWrite(Buzzer, 0);
  }
}
