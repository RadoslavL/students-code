#include <Adafruit_NeoPixel.h>
#include <SoftwareSerial.h>
#define rgbMatrix A0
#define VRx A1
#define VRy A7
#define SW A3
const int rgbMatrixPiexelCount = 64;
const int rgb_red = 8;
int rgb_index;
int ball_index = 40;
int ball_dir = -1;
Adafruit_NeoPixel rgb_Matrix = Adafruit_NeoPixel(rgbMatrixPiexelCount, rgbMatrix, NEO_GRB + NEO_KHZ800);
void setup() {
  Serial.begin(9600);
  rgb_Matrix.begin();
  rgb_Matrix.setBrightness(50);
  pinMode(VRx, INPUT);
  pinMode(VRy, INPUT);
  pinMode(SW, INPUT_PULLUP);
}

void loop() {
  //rgb_Matrix.clear();
  int x = analogRead(VRx);
  int y = analogRead(VRy);
  int btn = digitalRead(A7);
  int red = random(0, 256);
  int green = random(0, 256);
  int blue = random(0, 256);
  int rgb_x = map(x, 0, 860, 1, 7);
  int rgb_y = map(y, 0, 860, 0, 7);
  Serial.print(x);
  Serial.print(", ");
  Serial.print(y);
  Serial.print(", ");
  Serial.print(btn);
  Serial.println();

  {
    rgb_index = rgb_x * 8;
    rgb_Matrix.setPixelColor(rgb_index, red, green, blue);
    rgb_Matrix.setPixelColor(rgb_index + 8, red, green, blue);
    rgb_Matrix.setPixelColor(rgb_index - 8, red, green, blue);
    rgb_Matrix.show();
  }
  ball();
  delay(100);
  rgb_Matrix.clear();
 

}
void ball() {
  ball_index = ball_index + ball_dir;
  Serial.print("ball_index=");
  Serial.print(ball_index);
  Serial.print(" rgb_index=");
  Serial.println(rgb_index);
  if (ball_index == rgb_index) {
    ball_dir = 1;
  } 
  else if(ball_index == rgb_index - 8) {
    ball_dir = -7;
  }
  else {
    for (int i = 0; i < 8; i++) {
      if (i * 8 == ball_index) {
        ball_dir = -1;
        break;
      }
    }
  }
  ball_index = ball_index + ball_dir;
  rgb_Matrix.setPixelColor(ball_index, 255, 0, 0);
  rgb_Matrix.show();
  delay(1000);

}
