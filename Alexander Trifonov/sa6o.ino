#include <Adafruit_NeoMatrix.h>
#include <SoftwareSerial.h>
#define rgbMatrix A0
#define VRx A1
#define VRy A7
#define SW A3
const int rgbMatrixPiexelCount = 64;
const int rgb_red = 8;
int rgb_x = 3;
int rgb_y = 3;
Adafruit_NeoPixel rgb_Matrix = Adafruit_NeoPixel(rgbMatrixPiexelCount, rgbMatrix, NEO_GRB + NEO_KHZ800);
void setup() {
  Serial.begin(9600);
  rgb_Matrix.begin();
  rgb_Matrix.setBrightness(50);
  pinMode(VRx, INPUT);
  pinMode(VRy, INPUT);
  pinMode(SW, INPUT_PULLUP);
}

void loop() {

  int x = analogRead(VRx);
  int y = analogRead(VRy);
  int btn = digitalRead(A7);
  int red = random(0, 256);
  int green = random(0, 256);
  int blue = random(0, 256);

  Serial.print(x);
  Serial.print(", ");
  Serial.print(y);
  Serial.print(", ");
  Serial.print(btn);
  Serial.println();
  int rgb_index;
  if (x > 900) {
    rgb_Matrix.setPixelColor(rgb_x + 1, red, green, blue);
    rgb_Matrix.show();
    rgb_index = rgb_x * 8 + rgb_y;
  }
  rgb_Matrix.clear();
  if (x < 100) {

    rgb_Matrix.setPixelColor(rgb_x - 1, red, green, blue);
    rgb_Matrix.show();
    rgb_index = rgb_x * 8 + rgb_y;
  }
  rgb_Matrix.clear();
  if (y > 800) {
    rgb_Matrix.setPixelColor(rgb_y - 1, red, green, blue);
    rgb_Matrix.show();
    rgb_index = rgb_x * 8 + rgb_y;
  }
  rgb_Matrix.clear();
  if (y < 300) {
    rgb_Matrix.setPixelColor(rgb_y + 1, red, green, blue);
    rgb_Matrix.show();
    rgb_index = rgb_x * 8 + rgb_y;
  }
  rgb_Matrix.clear();

  rgb_Matrix.clear();
  delay(100);
}
