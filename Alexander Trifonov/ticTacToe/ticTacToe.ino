const int BOARD_SIZE = 3;
const char EMPTY = ' ';
const char X_SYMBOL = 'X';
const char O_SYMBOL = 'O';

// Function to determine the winner
char determineWinner(char board[BOARD_SIZE][BOARD_SIZE]) {
  // Check rows
  for (int i = 0; i < BOARD_SIZE; i++) {
    if (board[i][0] != EMPTY && board[i][0] == board[i][1] && board[i][0] == board[i][2]) {
      return board[i][0];
    }
  }

  // Check columns
  for (int j = 0; j < BOARD_SIZE; j++) {
    if (board[0][j] != EMPTY && board[0][j] == board[1][j] && board[0][j] == board[2][j]) {
      return board[0][j];
    }
  }

  // Check diagonals
  if (board[0][0] != EMPTY && board[0][0] == board[1][1] && board[0][0] == board[2][2]) {
    return board[0][0];
  }

  if (board[0][2] != EMPTY && board[0][2] == board[1][1] && board[0][2] == board[2][0]) {
    return board[0][2];
  }

  // No winner
  return EMPTY;
}

void setup() {
  Serial.begin(9600);

  // Example board with X and O positions
  char board[BOARD_SIZE][BOARD_SIZE] = {
    { X_SYMBOL, O_SYMBOL, EMPTY },
    { X_SYMBOL, X_SYMBOL, O_SYMBOL },
    { O_SYMBOL, X_SYMBOL, O_SYMBOL }
  };

  // Determine the winner
  char winner = determineWinner(board);

  if (winner == X_SYMBOL) {
    Serial.println("X wins!");
  } else if (winner == O_SYMBOL) {
    Serial.println("O wins!");
  } else {
    Serial.println("It's a draw!");
  }
}

void loop() {
}