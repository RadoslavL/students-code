#define Left_1 11
#define Left_2 5
#define Right_1 9
#define Right_2 10

int SPEED = 180;

#include <DHT.h>
#define DHT22_PIN 4

DHT sensor(DHT22_PIN, DHT22);
void setup() {
  // anton
  sensor.begin();
  Serial.begin(9600);
  pinMode(Left_1, OUTPUT);
  pinMode(Left_2, OUTPUT);
  pinMode(Right_1, OUTPUT);
  pinMode(Right_2, OUTPUT);
}

void loop() {
  int temp = sensor.readTemperature();
  Serial.println(temp);
  delay(100);

  forward();
  delay(2000);
  stopcar();
  left();
  delay(200);
  forward();
  delay(2000);
  stopcar();
  right();
  delay(200);
}

void forward() {
  analogWrite(Left_1, SPEED);
  digitalWrite(Left_2, LOW);
  analogWrite(Right_1, SPEED);
  digitalWrite(Right_2, LOW);

}

void reverse() {
  digitalWrite(Left_1, LOW);
  analogWrite(Left_2, SPEED);
  digitalWrite(Right_1, LOW);
  analogWrite(Right_2, SPEED);

}

void left() {
  digitalWrite(Left_1, LOW);
  analogWrite(Left_2, SPEED);
  analogWrite(Right_1, SPEED);
  digitalWrite(Right_2, LOW);

}

void right() {
  analogWrite(Left_1, SPEED);
  digitalWrite(Left_2, LOW);
  digitalWrite(Right_1, LOW);
  analogWrite(Right_2, SPEED);

}

void stopcar() {
  digitalWrite(Left_1, LOW);
  digitalWrite(Left_2, LOW);
  digitalWrite(Right_1, LOW);
  digitalWrite(Right_2, LOW);

}
