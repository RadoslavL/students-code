#include <Wire.h>
#include <Temperature_LM75_Derived.h>

#include "mega2560_pinout.h"

// Default address for I2C multiplexer TCA9548
#define TCA_ADDR 0x70
#define LM75A_TEMP_SENSOR_CHANNEL 0x02

#define LM75A_TEMP_SENSOR_ADDR 0x48

// TI LM75A is 9-bit (±0.5°C) temperature sensor
Generic_LM75 temperature(LM75A_TEMP_SENSOR_ADDR);

void I2C_MUX_Channel_Select(uint8_t ch) {
  if (ch > 7) return;

  Wire.beginTransmission(TCA_ADDR);
  Wire.write(1 << ch);
  Wire.endTransmission();
}

// Initialize all needed pins and libraries
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  
  // Default I2C pins are used on Mega2560 (only one I2C)
  Wire.begin();
  // Enable I2C and set MUX to LM75A on channel 2  
  I2C_MUX_Channel_Select(LM75A_TEMP_SENSOR_CHANNEL);  
}

// Write the code executed in a loop
void loop() {
  Serial.print("Temperature = ");
  Serial.print(temperature.readTemperatureC());
  Serial.println(" °C");

  delay(250);
}