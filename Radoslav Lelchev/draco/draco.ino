#include <Adafruit_NeoPixel.h>
#include <SoftwareSerial.h>

#define buzzer 6      //дефинираме пинове за бъзера и пиксела
#define idiot_pin A2  //дефинираме пинове за бъзера и пиксела
#define rx 2
#define tx 3

#define M1A 11
#define M1B 5  //дефинираме пиновете на моторите
#define M2A 9  //дефинираме пиновете на моторите
#define M2B 10

#define echo 8  //дефинитаме пиновете на ултразвуковия сенор
#define trig 7  //дефинитаме пиновете на ултразвуковия сенор

int distance = 0;
int input = 0;

Adafruit_NeoPixel idiot(1, idiot_pin, NEO_RGB + NEO_KHZ800);
SoftwareSerial rusaa(tx, rx);

int kiro() {
  int kiro = 0;
  /*
  for (int i = 0; i < 5; i++) {
    digitalWrite(trig, LOW);
    delay(2);
    digitalWrite(trig, HIGH);
    delay(2);
    digitalWrite(trig, LOW);
    kiro = kiro + pulseIn(echo, HIGH);
  }
  */
  digitalWrite(trig, LOW);
  delay(2);
  digitalWrite(trig, HIGH);
  delay(2);
  digitalWrite(trig, LOW);
  kiro = pulseIn(echo, HIGH);
  //kiro = kiro / 5;
  return (kiro / 50);
}

void forward() {
  digitalWrite(M1A, HIGH);
  digitalWrite(M1B, LOW);
  digitalWrite(M2A, HIGH);
  digitalWrite(M2B, LOW);
}

void backward() {
  digitalWrite(M1A, LOW);
  digitalWrite(M1B, HIGH);
  digitalWrite(M2A, LOW);
  digitalWrite(M2B, HIGH);
}

void left() {
  digitalWrite(M1A, LOW);
  digitalWrite(M1B, LOW);
  digitalWrite(M2A, HIGH);
  digitalWrite(M2B, LOW);
}

void right() {
  digitalWrite(M1A, HIGH);
  digitalWrite(M1B, LOW);
  digitalWrite(M2A, LOW);
  digitalWrite(M2B, LOW);
}

void backleft(){
  digitalWrite(M1A, LOW);
  digitalWrite(M1B, LOW);
  digitalWrite(M2A, LOW);
  digitalWrite(M2B, HIGH);  
}

void backright(){
  digitalWrite(M1A, LOW);
  digitalWrite(M1B, HIGH);
  digitalWrite(M2A, LOW);
  digitalWrite(M2B, LOW);
}

void movementreset() {
  digitalWrite(M1A, LOW);
  digitalWrite(M1B, LOW);
  digitalWrite(M2A, LOW);
  digitalWrite(M2B, LOW);
}

/*
void search() {
  if (time % 4 == 0) {
    left();
  }
  if (time % 8 == 0) {
    right();
  }
  if (time % 12 == 0) {
    forward();
  }
}
*/

void setup() {

  Serial.begin(9600);
  rusaa.begin(9600);

  pinMode(buzzer, OUTPUT);
  pinMode(trig, OUTPUT);
  pinMode(echo, INPUT);
  pinMode(M1A, OUTPUT);
  pinMode(M1B, OUTPUT);
  pinMode(M2A, OUTPUT);
  pinMode(M2B, OUTPUT);
  pinMode(idiot_pin, OUTPUT);
}

void loop() {
  if(Serial.available()){
    delay(1);
    rusaa.write(Serial.read());
  }
  if(rusaa.available()){
    input = rusaa.read();
    Serial.write(input);
    Serial.write("\n");
    if(input == 70){
      forward();
    }else if(input == 66){
      backward();
    }else if(input == 76){
      left();      
    }else if(input == 82){
      right();
    }else if(input == 71){
      left();
    }else if(input == 73){
      right();
    }else if(input == 72){
      backleft();
    }else if(input == 74){
      backright();
    }else if(input == 83){
      movementreset();
    }
  }
  distance = kiro();
  if (distance > 12 || distance == 0) {
    idiot.setPixelColor(0, 100, 0, 0);
    idiot.show();
    //movementreset();
  }
  if (distance < 12 && distance > 6) {
    idiot.setPixelColor(0, 100, 100, 0);
    idiot.show();
    //movementreset();
  }
  if (distance < 6 && distance != 0) {
    idiot.setPixelColor(0, 0, 100, 0);
    idiot.show();
    //forward();
  }
}