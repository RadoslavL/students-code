#include "mega2560_pinout.h"

// Initialize all needed pins and libraries
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial2.begin(9600);

  pinMode(SET_HC12_PIN, OUTPUT);
  pinMode(RED_LED_PIN, OUTPUT);
  pinMode(DIP1_PIN, INPUT);
}

char commandStart = 'v';
char commandOn = '1';
char commandOff = '0';

char myChar;

// Write the code executed in a loop
void loop() {
  bool pin1 = digitalRead(DIP1_PIN);

  if (pin1 == HIGH) {
    digitalWrite(SET_HC12_PIN, HIGH);
  } else {
    digitalWrite(SET_HC12_PIN, LOW);
  }

  if (Serial.available()) {
    Serial2.write(Serial.read());
  }

  if (Serial2.available()) {
    myChar = Serial2.read();
    Serial.write(myChar);
    if (myChar == commandStart) {
      if(Serial2.available()){
        myChar = Serial2.read();
      }
      Serial.write(myChar);
      if (myChar == commandOn) {
        digitalWrite(RED_LED_PIN, HIGH);
        Serial.print("on");
      } else if (myChar == commandOff) {
        digitalWrite(RED_LED_PIN, LOW);
        Serial.print("off");
      }else{
        Serial.println("Didn't work!");
      }
    }
  }
}
