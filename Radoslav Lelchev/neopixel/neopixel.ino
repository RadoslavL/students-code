#include <Adafruit_NeoPixel.h>
#include "mega2560_pinout.h"
Adafruit_NeoPixel rgbMatrix = Adafruit_NeoPixel(64, AP00, NEO_GRB + NEO_KHZ800);
unsigned char colors[8][8][3] = {255, 255, 255};
float x = 0.0f;
float y = 0.0f;
int velocityX = 511;
int velocityY = 511;
//unsigned char colors[8 * 8 * 3] = {255};
void setup(){
  Serial.begin(9600);
  rgbMatrix.begin();
  rgbMatrix.setBrightness(16);
}

void loop(){
  velocityX = digitalRead(JOYSTICK_X_PIN);
  velocityY = digitalRead(JOYSTICK_Y_PIN);


  clearscreen();
  if(x + velocityX < 10000 && x + velocityX > 0){
      x += velocityX;
  }
  if(y + velocityY < 10000 && y + velocityY > 0){
      y += velocityY;
  }
  for(int i = 0; i < 8; i++){
    for(int j = 0; j < 8; j++){
      if(j == x / 10000 * 8 && i == y / 10000 * 8){
        colors[i][j][0] = 255;
        colors[i][j][1] = 255;
        colors[i][j][2] = 255;
      }
    }
  }
  /*
  for(int i = 0; i < 8; i++){
    for(int j = 0; j < 8; j++){
      colors[i][j][0] = random(0, 127);
      colors[i][j][1] = 255;
      colors[i][j][2] = random(0, 127);
    }
  }
  */
  for(int i = 0; i < 8; i++){
    for(int j = 0; j < 8; j++){
      rgbMatrix.setPixelColor(i * 8 + j, colors[i][j][0], colors[i][j][1], colors[i][j][2]);
    }
  }
  rgbMatrix.show();
  Serial.print("x: ");
  Serial.write(x / 10000 * 8);
  Serial.print("y: ");
  Serial.write(y / 10000 * 8);
  delay(1000);
}

void clearscreen(){
  for(int i = 0; i < 8; i++){
    for(int j = 0; j < 8; j++){
      colors[i][j][0] = 0;
      colors[i][j][1] = 0;
      colors[i][j][2] = 0;
    }
  }
}
