#include <SoftwareSerial.h>
#include <VirtuinoBluetooth.h>
SoftwareSerial bt(3, 2);
VirtuinoBluetooth nashto(bt);

#define in1a 11
#define in1b 5
#define in2a 9
#define in2b 10

void stops() {  //функция за спиране
  digitalWrite(in1a, LOW);
  digitalWrite(in1b, LOW);
  digitalWrite(in2a, LOW);
  digitalWrite(in2b, LOW);
}

void forward(int sped) {  //функция за вървене напред
  analogWrite(in1a, sped);
  analogWrite(in1b, 0);
  analogWrite(in2a, sped);
  analogWrite(in2b, 0);
}

void setup() {
  Serial.begin(9600);
  bt.begin(9600);
  pinMode(in1a, OUTPUT);
  pinMode(in1b, OUTPUT);
  pinMode(in2a, OUTPUT);
  pinMode(in2b, OUTPUT);
}

void loop() {
  bt.listen();
  while (bt.available() == 0) {
    stops();
  }

  while (bt.available()) {

    int rawspeed = bt.read();  //четем стойността която получваме от блутута
    if (rawspeed <= '9' && '0' >= rawspeed or rawspeed == 'q') {
      int relativspeed = rawspeed % 103;            //ако получим q то се прервръща в 113 и 113%103 = 10
      int sped = map(relativspeed, 0, 10, 0, 253);  //Превръщаме стойностите на скоростта от тези на приложението до тези на мотора
      forward(sped);                                //задаваме скоросТТА НА МОТОРИТЕ
      Serial.println(sped);
      delay(200);
    }
  }
}