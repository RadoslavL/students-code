#include <Adafruit_NeoPixel.h>

#define BUZZER 6
#define Button 12
#define RedPedestrians 13
#define RGB_Matrix A3
#define LDR A6

int light;
int i;
void RedCars();
void YellowCars();
void GreenCars();
void BlinkCars();
void GoPedestrians();
void Warning();
void StopPedestrians();

Adafruit_NeoPixel pixels_RGB_Matrix = Adafruit_NeoPixel(64, RGB_Matrix, NEO_GRB + NEO_KHZ800);


//200>нормална светлина<300-600>тъмнина<800
void setup() {
  pinMode(BUZZER, OUTPUT);  
  pinMode(Button, INPUT); 
  pinMode(RedPedestrians, OUTPUT);
  pinMode(RGB_Matrix, OUTPUT);
  pinMode(LDR, INPUT);
  pixels_RGB_Matrix.begin();
  pixels_RGB_Matrix.setBrightness(70);
  GreenCars();
  digitalWrite(RedPedestrians, HIGH);
  digitalWrite(BUZZER, LOW);
}

void loop() {
  light = analogRead(LDR);
  if (light < 500) {
    GreenCars();
    if (digitalRead(Button) == HIGH) {
      delay(3000);
      YellowCars();
      delay(600);
      BlinkCars();
      delay(100);
      YellowCars();
      delay(100);
      BlinkCars();
      delay(100);
      YellowCars();
      delay(100);
      RedCars();
      GoPedestrians();
      delay(4000);

      Warning();
      StopPedestrians();
      delay(1000);
      GreenCars();
    }

  }
  else {
    digitalWrite(RedPedestrians, LOW);
    digitalWrite(BUZZER, LOW);
    YellowCars();
    delay(500);
    BlinkCars();
    delay(500);
  }
}
void RedCars(){
      for(int i = 0;i < 64;i++){
        if(i < 16){
          pixels_RGB_Matrix.setPixelColor(i, 255, 0, 0);
        }
        else{
          pixels_RGB_Matrix.setPixelColor(i, 0, 0, 0);
        }
      }
      pixels_RGB_Matrix.show();
}
void YellowCars(){
      
      for(int i = 0;i < 64;i++){
        if(23 < i && i < 40){
          pixels_RGB_Matrix.setPixelColor(i, 255, 255, 0);
        }
        else{
          pixels_RGB_Matrix.setPixelColor(i, 0, 0, 0);
        }
      };
      pixels_RGB_Matrix.show();
      }
void GreenCars(){
      for(int i = 0;i < 64;i++){
        if(47 < i){
          pixels_RGB_Matrix.setPixelColor(i, 0, 255, 0);
        }
        else{
          pixels_RGB_Matrix.setPixelColor(i, 0, 0, 0);
        }
      }
      pixels_RGB_Matrix.show();
}
void BlinkCars(){
      for(int i = 0;i < 64;i++){
      pixels_RGB_Matrix.setPixelColor(i, 0, 0, 0);
      }
      pixels_RGB_Matrix.show();
}
void GoPedestrians(){
      digitalWrite(RedPedestrians, LOW);
      digitalWrite(BUZZER, HIGH);
}
void Warning(){
      YellowCars();
      delay(300);
      BlinkCars();
      digitalWrite(RedPedestrians, HIGH);
      digitalWrite(BUZZER, LOW);
      delay(150);
      YellowCars();
      digitalWrite(RedPedestrians, LOW);
      digitalWrite(BUZZER, HIGH);
      delay(300);
      BlinkCars();
      digitalWrite(RedPedestrians, HIGH);
      digitalWrite(BUZZER, LOW);
      delay(150);
      YellowCars();
      digitalWrite(RedPedestrians, LOW);
      digitalWrite(BUZZER, HIGH);
      delay(100);
}
void StopPedestrians(){
      digitalWrite(RedPedestrians, HIGH);
      digitalWrite(BUZZER, LOW);
}
