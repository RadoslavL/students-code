#define VRx A3
#define VRy A0
#define Button A1
void setup() {
  Serial.begin(9600);
  pinMode(VRx, INPUT);
  pinMode(VRy, INPUT);
  pinMode(Button, INPUT_PULLUP);
}

void loop() {
  int vrx = analogRead(VRx);
  int vry = analogRead(VRy);
  int button = digitalRead(Button);
  Serial.print(vrx);
  Serial.print(",");
  Serial.print(vry);
  Serial.print(",");
  Serial.println(button);
  delay(1000);
}
