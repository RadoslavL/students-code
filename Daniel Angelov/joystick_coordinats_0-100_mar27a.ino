#define VRx A3
#define VRy A0
#define Button A1
void setup() {
  Serial.begin(9600);
  pinMode(VRx, INPUT);
  pinMode(VRy, INPUT);
  pinMode(Button, INPUT_PULLUP);
  int vrx = analogRead(VRx);
  int vry = analogRead(VRy);
  Serial.println("center:");
  Serial.print(vrx);
  Serial.print(",");
  Serial.print(vry);
  Serial.println( );
  Serial.println( );
  Serial.println( );
  Serial.println( );
}

void loop() {
  int vrx = analogRead(VRx);
  int vry = analogRead(VRy);
  int button = digitalRead(Button);
  vrx = map(vrx,0,869,0,7);
  vry = map(vry,0,869,0,7);
  Serial.print(vrx);
  Serial.print(",");
  Serial.print(vry);
  Serial.print(",");
  Serial.println(button);
  delay(1000);
}
