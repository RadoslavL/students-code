#include <TM1637Display.h>
#include "mega2560_pinout.h"
TM1637Display display(SEGMENT_DISPLAY_CLK_PIN, SEGMENT_DISPLAY_DIO_PIN);
int sec = 0;
int min = 0;
int clock = 0;
void setup() {
  display.setBrightness(7);
  display.clear();
  //display.showNumberDec(sec, 1);
  //display.showNumberDec(min, 1, 2, 1);
}

void loop() {
  clock = min + sec;
  display.showNumberDecEx(clock, 0b11100000,  1);
  sec = sec + 1;
  if (sec == 60) {
    sec = 0;
    min = min + 100;
  }
  delay(1000);
}
