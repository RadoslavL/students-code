#define BT_RX 3
#define BT_TX 2
#define LED_PIN 13

#include <SoftwareSerial.h>
SoftwareSerial bluetooth(BT_RX, BT_TX);

#include "VirtuinoBluetooth.h"
VirtuinoBluetooth virtuino(bluetooth);

void setup() {
  bluetooth.begin(9600);
  Serial.begin(9600);
  pinMode(LED_PIN, OUTPUT);

}

void loop() {
  if (bluetooth.available()) {
    Serial.write(bluetooth.read());
  }
  if (Serial.available()) {
    bluetooth.write(Serial.read());
  } 
  virtuino.run();
  if(virtuino.vMemoryRead(1) == 1) {
    digitalWrite(LED_PIN, HIGH);
  }
  else{
    digitalWrite(LED_PIN, LOW);
  }
}
