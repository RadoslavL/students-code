#include <Adafruit_NeoPixel.h>
#define RGB_MATRIX_PIN A3
Adafruit_NeoPixel RGB_MATRIX = Adafruit_NeoPixel (64,RGB_MATRIX_PIN,NEO_GRB + NEO_KHZ800 );
void setup() {


}

void loop() {
   int matrix[64][3];
  for (int index=0; index<64; index++){
    for (int rgb=0; rgb<3; rgb++){
      matrix[index][rgb]=random(0,256);
    }
  }
  
  RGB_MATRIX.begin();
  RGB_MATRIX.setBrightness(random(0,256));
  
  for (int index=0; index<64; index++){
    RGB_MATRIX.setPixelColor(index, matrix[index][0],matrix[index][1],matrix[index][2]);
  }
  
  RGB_MATRIX.show();
  delay(2000);
}
