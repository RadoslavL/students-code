#include <Wire.h>
#include <Temperature_LM75_Derived.h>
#include "mega2560_pinout.h"
#define TCA_ADDR 0x70 
#define LM75A_TEMP_SENSOR_ADDR 0x48
Generic_LM75 TEMP (LM75A_TEMP_SENSOR_ADDR);
void setup() {
Wire.begin();
Wire.beginTransmission(TCA_ADDR);
Wire.write(4);
Wire.endTransmission();
Serial.begin(9600);
}

void loop() {
Serial.print("Temperature = ");
Serial.print(TEMP.readTemperatureC());
Serial.println("°C");
delay(1000);
}
