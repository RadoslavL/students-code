#include <Adafruit_Sensor.h>
#include <DHT.h>

#define DRAKO

#ifdef DRAKO
  #define LDR_PIN A6 
  #define DHT22_pin 4
  #define buzzer 6
#else
  #define LDR_PIN A5 
  #define DHT22_pin 4
  #define buzzer 2
#endif

DHT sensor(DHT22_pin,DHT22);
void setup() {
  sensor.begin();
  pinMode(LDR_PIN,INPUT);
  Serial.begin(9600);
  pinMode(buzzer,OUTPUT);
}

void loop() {
  int temp=sensor.readTemperature();
  int humidity=sensor.readHumidity();
  int light=analogRead(LDR_PIN);
  
  delay(1000);
  Serial.print("light=");
  Serial.println(light);
  Serial.print("temp=");
  Serial.println(temp);
  Serial.print("humidity=");
  Serial.println(humidity);
  if ((temp>21) && (light>400) && (humidity>55)){
    digitalWrite(buzzer,HIGH);
  }
  else {
    digitalWrite(buzzer,LOW);
  }
} 
