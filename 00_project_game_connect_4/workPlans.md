# План за работа 

## 01 занятие 2024.10.05

1. Запознаване на групата с проекта

## 02 занятие 2024.10.12

1. Проверка на 3D дизайн
    - отвори за виждане на пуловете не са разположени добре във всички колони
1. Електроника
    - обсъждане на подобрение на запояване на сензорите и държачи за тях 

## 03 занятие 2024.10.19

1. Електроника
    - тестово решение: крачетата на сензора се отчупват лесно, да се тества може ли да се запои на платка и тогава да се слагат кабели?
    - резистори за свързване към сензорите, да се уточнят стойностите и да се подготви върху платка 
1. 3D дизайн
    - проектиране на нов държач за сензор
    - отпечатване на прототип за един сензор 
    - да се направи запои на един сензор + резистори + изводи за свързване към платка


Необходими материали:
- прототипна платка(еднослойна), от която да се изрязват парчета
- режещи инструменти
    - резци малки и големи
    - трион
- шкурка, пила за изглаждане на ръбове 
- поялник
- тинол
- жици
- сензори 2-3 за тест + 7бр за реално използване
- резистори 2вида x 7бр. (да се уточнят стойностите)
- 3D принтер
- филамент 

Резултат:
- дизайн на ново закрепване на сензорите, първи пробен печат. Необходима е корекция...

## 04 занятие 26.10.2024

1. Електроника
    - резистори за свързване към сензорите, да се уточнят стойностите и да се подготви върху платка 
1. 3D дизайн
    - корекция на нов държач за сензор
    - отпечатване на прототип за един сензор 
    - да се направи запои на един сензор + резистори + изводи за свързване към платка


Необходими материали:
- прототипна платка(еднослойна), от която да се изрязват парчета
- режещи инструменти
    - резци малки и големи
    - трион
- шкурка, пила за изглаждане на ръбове 
- поялник
- тинол
- жици
- сензори 2-3 за тест + 7бр за реално използване
- резистори 2вида x 7бр. (да се уточнят стойностите)
- 3D принтер
- филамент 

Резултат:  
- избрани резистори след като тествахме сензор R = 100 Ohm и R<sub>L</sub> = 2.2k Ohm 
- резултатния диапазон за обект и без обект е 150 и 910 (да се калибрира за дадена платка и да се прецени грешка +/- 30 дали е достатъчна)

## 05 занятие 09.11.2024

1. Електроника
    - резистори за свързване към сензорите, да се запоят върху платка 
1. 3D дизайн
    - корекция на нов държач за сензор
    - отпечатване на прототип за един сензор 


Необходими материали:
- прототипна платка(еднослойна), от която да се изрязват парчета
- режещи инструменти
    - резци малки и големи
    - трион
- шкурка, пила за изглаждане на ръбове 
- поялник
- тинол
- жици
- сензори 2-3 за тест + 7бр за реално използване
- резистори 2вида x 7бр. (да се уточнят стойностите)
- 3D принтер
- филамент 

Резултат:  
- избрани резистори след като тествахме сензор R = 100 Ohm и R<sub>L</sub> = 2.2k Ohm 
- резултатния диапазон за обект и без обект е 150 и 910 (да се калибрира за дадена платка и да се прецени грешка +/- 30 дали е достатъчна)

## 06 занятие

## 07 занятие

## 08 занятие

## 09 занятие

## 10 занятие

## 11 занятие

## 12 занятие