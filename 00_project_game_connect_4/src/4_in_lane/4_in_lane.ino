#define sensor0pin A0
#define sensor1pin A1
#define sensor2pin A2
#define sensor3pin A3
#define sensor4pin A4
#define sensor5pin A5
#define sensor6pin A6
int board[7][6];
int counter0;
const int treshhold_low = 30;
const int treshhold_high = 120;
const int hallow_solid_piece_border = 450;  //should find the border between the passing time of the solid piece and the hallow one and change the value
unsigned long start_time = 0;
unsigned long end_time = 0;
unsigned long object_time = 0;
bool solidity = 0;

bool object = 0;
//
void check(int sensorpin) {
  int value = analogRead(sensorpin);

  if (value < treshhold_low && object == 0) {
    start_time = micros();
    object = 1;
  }
  
  if (value > treshhold_high) {
    object_time = 0;
    if (object == 1) {
      end_time = micros();
      object_time = end_time - start_time;
      object = 0;
    }
  }
}

void piece_check() {
  if(object_time > hallow_solid_piece_border){
    solidity = 1;
  }
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println(sizeof(unsigned long));
}

void loop() {
  // put your main code here, to run repeatedly:
}
