#include <Adafruit_Sensor.h>
#include<Adafruit_NeoPixel.h>
#include<DHT.h>

#define temp_low 23
#define temp_med 25
#define temp_high 28
Adafruit_NeoPixel rgb(1, 16, NEO_GRB + NEO_KHZ800);

DHT sensor(4, DHT22);

void setup() {
  sensor.begin();
  rgb.begin();
  rgb.clear();
  Serial.begin(9600);

}

void loop() {
  int temp = sensor.readTemperature();


  Serial.println(temp);
  delay(1000);

  if (temp <= temp_low) {
  rgb.setPixelColor(0,0,0,255);
  rgb.show();
  }
  else if (temp_low < temp && (temp <= temp_med)) {
  rgb.setPixelColor(0,0,255,0);
  rgb.show();
  }
  else if ((temp_med < temp) && (temp <= temp_high)) {
  rgb.setPixelColor(0,125,125,0);
  rgb.show();
  }
  else if (temp_high < temp) {
  rgb.setPixelColor(0,255,0,0);
  rgb.show();
  } 
}
