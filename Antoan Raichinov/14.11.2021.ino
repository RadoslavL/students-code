#define Motor_A1 11
#define Motor_A2 5
#define Motor_B3 9
#define Motor_B4 10
#define BT_RX 3
#define BT_TX 2

#include <SoftwareSerial.h>
SoftwareSerial bluetoothSerial(BT_RX, BT_TX);

#include "VirtuinoBluetooth.h"
VirtuinoBluetooth virtuino(bluetoothSerial);


void setup() {
  bluetoothSerial.begin(9600);
  pinMode(Motor_A1, OUTPUT);
  pinMode(Motor_A2, OUTPUT);
  pinMode(Motor_B3, OUTPUT);
  pinMode(Motor_B4, OUTPUT);
}

void loop() {
  virtuino.run();

  if (virtuino.vMemoryRead(1) == 1)
  {
    motors_on();
  }
  else {
    motors_stop();
  }
    if (virtuino.vMemoryRead(2) == 1)
  {
    motors_left();
  }
  else {
    motors_stop();
  }
    if (virtuino.vMemoryRead(3) == 1)
  {
    motors_right();
  }
  else {
    motors_stop();
  }
    if (virtuino.vMemoryRead(4) == 1)
  {
    motors_back();
  }
  else {
    motors_stop();
  }
}
  void motors_on ()
  {
    digitalWrite(Motor_A1, HIGH);
    digitalWrite(Motor_A2, LOW);
    digitalWrite(Motor_B3, HIGH);
    digitalWrite(Motor_B4, LOW);
  }

  void motors_back()
  {
    digitalWrite(Motor_A1, LOW);
    digitalWrite(Motor_A2, HIGH);
    digitalWrite(Motor_B3, LOW);
    digitalWrite(Motor_B4, HIGH);
  }

  void motors_stop()
  {
    digitalWrite(Motor_A1, LOW);
    digitalWrite(Motor_A2, LOW);
    digitalWrite(Motor_B3, LOW);
    digitalWrite(Motor_B4, LOW);
  }

  void motors_left()
  {
    digitalWrite(Motor_A1, LOW);
    digitalWrite(Motor_A2, HIGH);
    digitalWrite(Motor_B3, HIGH);
    digitalWrite(Motor_B4, LOW);
  }

  void motors_right()
  {
    digitalWrite(Motor_A1, HIGH);
    digitalWrite(Motor_A2, LOW);
    digitalWrite(Motor_B3, LOW);
    digitalWrite(Motor_B4, HIGH);
  }
