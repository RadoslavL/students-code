
#include "mega2560_pinout.h"

void setup() {

  Serial2.begin(9600);
  pinMode(SET_HC12_PIN, OUTPUT);
  pinMode(DIP1_PIN, INPUT);
  Serial.begin(9600);
  
}

void loop() {

  int i = digitalRead(DIP1_PIN);
  
  digitalWrite(SET_HC12_PIN, i);

  if (Serial.available()) {

  Serial2.write(Serial.read());

  }

  if (Serial2.available()) {

  Serial.write(Serial2.read());

  }
}
